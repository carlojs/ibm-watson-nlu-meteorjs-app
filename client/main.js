import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';

import './main.html';

Template.url.onCreated(() => {
    Template.instance().url = new ReactiveVar(0);
    Template.instance().categories = new ReactiveVar(false);
    Template.instance().console = new ReactiveVar(false);
});

Template.url.helpers({
    categories() {
        return Template.instance().categories.get();
    },
    console(){
        return Template.instance().console.get();
    }
});

Template.url.events({
    'change input' (event, instance) {
        instance.url.set(event.target.value);
    },
    'click button' (event, instance) {
        // Regexp to check that url provided is valid
        const expression = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
        const regex = new RegExp(expression);
        let url = instance.url.get();

        instance.categories.set();
        instance.console.set('Fetching the data...');

        if (url && url.match(regex)) {
            // protocol is removed from url
            url = url.replace(/(^\w+:|^)\/\//, '');

            Meteor.call('api.watson-nlu', url, (error, result) => {
                if (result) {
                    instance.console.set();
                    instance.categories.set(result.data.categories);
                }else{
                    instance.console.set('Something went wrong, try later.');
                }
            });
        }else{
            instance.console.set('Please insert a valid url.');
        }
    }
});
