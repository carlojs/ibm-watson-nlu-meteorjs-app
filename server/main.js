import {
    HTTP
} from 'meteor/http';

import {
    Promise
} from 'meteor/promise';


const getCategories = (url, limit) => {
    return new Promise((resolve, reject) => {
        HTTP.call('POST', 'https://gateway-lon.watsonplatform.net/natural-language-understanding/api/v1/analyze', {
           "auth": "apikey:WKDN2Z1NBRSTiN5ewc4jVPaVrIvEZqOG-zrOX5e3Xbgt",// should be parametrized
           "headers": "Content-Type: application/json",
           "data": {
               "url": url,
               "version":"2018-03-19",
               "features": {
                   "categories": {
                       "limit": limit
                   }
               }
           }
       }, (error, result) => {
           if (!error) {
               resolve(result);
           }else{
               reject(error);
           }
       });
    });
};

Meteor.methods({
    'api.watson-nlu' : async (url) => {
        return await getCategories(url, 3);
    }
});
