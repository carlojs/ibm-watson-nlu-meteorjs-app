# MeteorJS Url Taxonomy Application
Basic MeteorJS application which allows the user to submit a website URLs and returns the site taxonomy, fetched from the IMB Watson API - Natural Language Understanding endpoint.

## Installation
meteor npm install

## Build & Run
meteor [--port xyz]
